export interface TCompany {
  id: number;
  title?: string;
  public_title?: string;
  short_desc?: string;
  logo?: string;
  contry_id: number;
  city: string;
  active?: number;
  phone?: string;
  //and more
}
