export interface TResponse<T = unknown> {
  success: boolean
  data: T
  meta: {
    [key: string]: unknown
    message?: string
  }
}
