export enum Routes {
  Welcome = "/",
  Error = "/error",
  SelectCity = "/select-city",
  InitService = "/init-service",
  SelectCompany = "/select-company",
  SelectZone = "/select-zone",
  SelectDate = "/select-date",
  SMSConfirm = "/sms-confirm",
  Success = "/success",
  Page404 = "/404",
  Preparation = "/preparation",
}
